# Sputnik
A app for iPhone for coding related task managment stuff. <br>
Use Xcode in macOS X to emulate app.<br> **Emulator should support iPhone 7, 7 Plus, 8 and 8 Plus.**<br> 
<br>
<br>
  <a href="https://saythanks.io/to/KartSriv">
      <img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg">
  </a>
</p>

[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://github.com/KartSriv/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://github.com/KartSriv/)
